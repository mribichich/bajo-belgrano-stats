import batting from 'csv/2019/1117/statsVisitorBatting.json';
import fielding from 'csv/2019/1117/statsVisitorFielding.json';
import pitching from 'csv/2019/1117/statsVisitorPitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20191117',
  date: new Date('2019-11-17T13:30:00.000Z'),
  title: '2019/11/17 at Daom',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
