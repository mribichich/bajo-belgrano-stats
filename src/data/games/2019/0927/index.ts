import batting from 'csv/2019/0927/statsHomeBatting.json';
import fielding from 'csv/2019/0927/statsHomeFielding.json';
import pitching from 'csv/2019/0927/statsHomePitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20190927',
  date: new Date('2019-09-27T13:30:00.000Z'),
  title: '2019/09/27 at Velez U23',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
