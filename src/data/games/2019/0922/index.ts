import batting from 'csv/2019/0922/statsHomeBatting.json';
import fielding from 'csv/2019/0922/statsHomeFielding.json';
import pitching from 'csv/2019/0922/statsHomePitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20190922',
  date: new Date('2019-09-22T13:30:00.000Z'),
  title: '2019/09/22 at Tigres',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
