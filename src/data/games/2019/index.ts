import game0901 from './0901';
import game0908 from './0908';
import game0922 from './0922';
import game0927 from './0927';
import game1103 from './1103';
import game1110 from './1110';
import game1117 from './1117';
import game1124 from './1124';

export default [game0901, game0908, game0922, game0927, game1103, game1110, game1117, game1124];
