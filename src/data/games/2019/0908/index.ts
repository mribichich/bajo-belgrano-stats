import batting from 'csv/2019/0908/statsVisitorBatting.json';
import fielding from 'csv/2019/0908/statsVisitorFielding.json';
import pitching from 'csv/2019/0908/statsVisitorPitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20190908',
  date: new Date('2019-09-08T13:30:00.000Z'),
  title: '2019/09/08 at Black Sox',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
