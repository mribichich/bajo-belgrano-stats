import batting from 'csv/2019/0901/statsVisitorBatting.json';
import fielding from 'csv/2019/0901/statsVisitorFielding.json';
import pitching from 'csv/2019/0901/statsVisitorPitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20190901',
  date: new Date('2019-09-01T15:00:00.000Z'),
  title: '2019/09/01 at Ferro',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
