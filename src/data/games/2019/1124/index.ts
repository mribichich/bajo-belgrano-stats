import batting from 'csv/2019/1124/statsHomeBatting.json';
import fielding from 'csv/2019/1124/statsHomeFielding.json';
import pitching from 'csv/2019/1124/statsHomePitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20191124',
  date: new Date('2019-11-24T13:30:00.000Z'),
  title: '2019/11/24 vs DBacks',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
