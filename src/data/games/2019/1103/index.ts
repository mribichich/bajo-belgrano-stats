import batting from 'csv/2019/1103/statsHomeBatting.json';
import fielding from 'csv/2019/1103/statsHomeFielding.json';
import pitching from 'csv/2019/1103/statsHomePitching.json';
import { BattingStats } from 'entities/battingStats';
import { Game } from 'entities/game';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  id: '20191103',
  date: new Date('2019-11-03T13:30:00.000Z'),
  title: '2019/11/03 vs DBacks',
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
} as Game;

export default game;
