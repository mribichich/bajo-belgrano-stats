import batting from 'csv/2019/statsBatting.json';
import fielding from 'csv/2019/statsFielding.json';
import pitching from 'csv/2019/statsPitching.json';
import { BattingStats } from 'entities/battingStats';
import { FieldingStats } from 'entities/fieldingStats';
import { PitchingStats } from 'entities/pitchingStats';

const game = {
  batting: batting as BattingStats[],
  fielding: fielding as FieldingStats[],
  pitching: pitching as PitchingStats[],
};

export default game;
