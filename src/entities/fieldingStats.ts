export type FieldingStats = {
  '#': number;
  Name: string;
  G: number;
  Et: number;
  Ef: number;
  ERR: number;
  PO: number;
  A: number;
  SBA: number;
  CS: number;
  DP: number;
  TP: number;
  PB: number;
  PKF: number;
  PK: number;
  FP: number;
};
