import { BattingStats } from './battingStats';
import { FieldingStats } from './fieldingStats';
import { PitchingStats } from './pitchingStats';

export type Game = {
  id: string;
  date: Date;
  title: string;
  batting: BattingStats[];
  fielding: FieldingStats[];
  pitching: PitchingStats[];
};
