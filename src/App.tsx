import React, { useState } from 'react';
import BattingTableStats from './components/BattingTableStats/BattingTableStats';
import games from 'data/games';
import allStats from 'data/allStats';
import GamesSelect from 'components/GamesSelect/GamesSelect';
import { Game } from 'entities/game';
import { sort, descend, head, length, prop, take, map, dropLast } from 'ramda';
import Select from '@material-ui/core/Select';
import { MenuItem, TextField } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { BattingStats } from 'entities/battingStats';
import StatsCard from 'components/StatsCard/StatsCard';

const ALL = 'ALL';
type ALL = typeof ALL;
const BY_GAME = 'BY_GAME';
type BY_GAME = typeof BY_GAME;

const gamesSorted = sort(descend(s => s.date), games);

const App: React.FC = () => {
  const [statsType, setStatsType] = useState<ALL | BY_GAME>('ALL');
  const [game, setGame] = useState(head(gamesSorted));
  const [minPa, setMinPa] = useState(length(gamesSorted));

  function handleStatsType(event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>) {
    setStatsType(event.target.value as ALL | BY_GAME);
  }

  function handleGameChange(event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>) {
    setGame(gamesSorted.find(f => f.id === event.target.value) as Game);
  }

  function handleMinPAChange(event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>) {
    setMinPa(event.target.value as number);
  }

  const stats = statsType === ALL ? allStats : game;

  const cardsData =
    stats &&
    ['R', 'H', 'HR', 'RBI', 'AVG', 'BB', 'SB', 'OBP', 'SLG', 'OPS'].map(key => {
      const keyStats = map(m => ({ number: m['#'], name: m.Name, PA: m.PA, value: m[key as keyof BattingStats] as number }), sort(
        descend(prop(key)),
        dropLast(1, stats.batting)
      ) as BattingStats[]);

      return { key, stats: keyStats };
    });

  return (
    <div>
      <TextField select style={{ minWidth: 120 }} label="Mostrar" value={statsType} onChange={handleStatsType}>
        <MenuItem value={ALL}>Todos</MenuItem>
        <MenuItem value={BY_GAME}>Por Juego</MenuItem>
      </TextField>

      {statsType === ALL && (
        <TextField
          style={{ width: 80 }}
          type="number"
          label="Min PA"
          value={minPa}
          onChange={handleMinPAChange}
          inputProps={{ min: 0 }}
        ></TextField>
      )}

      {statsType === BY_GAME && game && <GamesSelect games={gamesSorted} gameId={game.id} onChange={handleGameChange}></GamesSelect>}

      <br />
      <br />

      {cardsData && (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
          {cardsData.map(m => (
            <StatsCard title={m.key} stats={m.stats} minPa={statsType === ALL ? minPa : undefined}></StatsCard>
          ))}
        </div>
      )}

      <br />
      <br />

      {stats && <BattingTableStats stats={stats.batting} minPa={statsType === ALL ? minPa : undefined}></BattingTableStats>}
    </div>
  );
};

export default App;
