import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { BattingStats } from 'entities/battingStats';
import { ascend, descend, prop, sort, pipe, curry, filter, dropLast, partition, not, isNil, isEmpty } from 'ramda';
import React, { useState } from 'react';
import grey from '@material-ui/core/colors/grey';

type Order = 'asc' | 'desc';
const ASC = 'asc';
const DESC = 'desc';

const defaultHeaders = [
  'G',
  'PA',
  'AB',
  'R',
  'H',
  'B',
  '1B',
  '2B',
  '3B',
  'HR',
  'RBI',
  'AVG',
  'BB',
  'Kc',
  'Ks',
  'SO',
  'HBP',
  'SB',
  'CS',
  'SCB',
  'SF',
  'SAC',
  'RPA',
  'OBP',
  'OBPE',
  'SLG',
  'OPS',
  'GPA',
  'CT%',
  'CT2%',
  'ROE',
  'FC',
  'CI',
  'GDP',
  'GTP',
  'AB/RSP',
  'H/RSP',
  'BA/RSP',
  'QAB1%',
  'QAB2%',
  'QAB3%',
];

type Props = {
  stats: BattingStats[];
  minPa?: number;
};

export default function BattingTableStats({ stats, minPa = 0 }: Props) {
  const [order, setOrder] = useState<Order>(DESC);
  const [orderBy, setOrderBy] = useState<keyof BattingStats>('AVG');

  function handleRequestSort(property: keyof BattingStats) {
    return (event: React.MouseEvent<unknown>) => {
      const isDesc = orderBy === property && order === DESC;
      setOrder(isDesc ? ASC : DESC);
      setOrderBy(property);
    };
  }

  const sortFunc = order === ASC ? ascend<any>(prop(orderBy)) : descend<any>(prop(orderBy));
  const rows = sort(sortFunc, dropLast(1, stats));
  const [gtMinPa, ltMinPa] = partition(f => f.PA >= minPa, rows);

  return (
    <div>
      <BattingTable rows={gtMinPa} orderBy={orderBy} order={order} handleRequestSort={handleRequestSort}></BattingTable>

      {not(isEmpty(ltMinPa)) && (
        <>
          <br></br>
          <br></br>

          <BattingTable rows={ltMinPa} orderBy={orderBy} order={order} handleRequestSort={handleRequestSort}></BattingTable>
        </>
      )}
    </div>
  );
}

type BattingTableProps = {
  rows: BattingStats[];
  orderBy: keyof BattingStats;
  order: Order;
  handleRequestSort: (property: keyof BattingStats) => (event: React.MouseEvent<unknown>) => void;
};

function BattingTable({ rows, orderBy, order, handleRequestSort }: BattingTableProps) {
  return (
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell padding="none"></TableCell>
          <TableCell align="right" style={{ paddingRight: 16 }} sortDirection={orderBy === '#' ? order : false} padding="none">
            <TableSortLabel active={orderBy === '#'} direction={order} onClick={handleRequestSort('#' as keyof BattingStats)}>
              #
            </TableSortLabel>
          </TableCell>
          <TableCell style={{ minWidth: 130 }} sortDirection={orderBy === 'Name' ? order : false} padding="none">
            <TableSortLabel active={orderBy === 'Name'} direction={order} onClick={handleRequestSort('Name' as keyof BattingStats)}>
              Name
            </TableSortLabel>
          </TableCell>
          {defaultHeaders.map(m => (
            <TableCell key={m} align="right" sortDirection={orderBy === m ? order : false} padding="none">
              <TableSortLabel active={orderBy === m} direction={order} onClick={handleRequestSort(m as keyof BattingStats)}>
                {m}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row, index) => (
          <TableRow key={row.Name}>
            <TableCell style={{ paddingRight: 16 }} padding="none">
              {index + 1}
            </TableCell>
            <TableCell style={{ paddingRight: 16 }} align="right" padding="none">
              {row['#']}
            </TableCell>
            <TableCell component="th" scope="row" padding="none">
              {row.Name}
            </TableCell>
            {defaultHeaders.map(m => (
              <TableCell key={m} align="right" padding="none" style={m === orderBy ? { backgroundColor: grey[200] } : undefined}>
                {row[m as keyof BattingStats]}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}
