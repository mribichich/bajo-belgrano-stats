import React from 'react';
import TextField from '@material-ui/core/TextField';
import { MenuItem } from '@material-ui/core';
import { Game } from 'entities/game';

type Props = {
  games: Game[];
  gameId: string | undefined;

  onChange: (event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>) => void;
};

export default function GamesSelect({ games, gameId, onChange }: Props) {
  return (
    <TextField style={{ minWidth: 210 }} select label="Juego" value={gameId} onChange={onChange}>
      {games.map(m => (
        <MenuItem key={m.id} value={m.id}>
          {m.title}
        </MenuItem>
      ))}
    </TextField>
  );
}
