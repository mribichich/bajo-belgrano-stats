import React from 'react';
import Select from '@material-ui/core/Select';
import { MenuItem, TextField } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { filter, take, drop } from 'ramda';
import head from 'ramda/es/head';

type Props = {
  title: string;
  stats: {
    number: number;
    name: string;
    PA: number;
    value: number;
  }[];
  minPa?: number;
};

export default function StatsCard({ title, stats, minPa = 0 }: Props) {
  const filteredStats = take(5, filter(f => f.PA >= minPa && f.value > 0, stats));

  return (
    <div style={{ margin: 4 }}>
      <Card style={{ height: '100%' }}>
        <CardContent>
          <Typography component="h4" gutterBottom>
            {title}
          </Typography>
          <Typography variant="body2" component="p">
            <Table size="small" aria-label="stats card">
              <TableBody>
                {take(1, filteredStats).map(m => (
                  <Row {...m} strong={true}></Row>
                ))}
                {drop(1, filteredStats).map(m => (
                  <Row {...m}></Row>
                ))}
              </TableBody>
            </Table>
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}

type RowProps = {
  number: number;
  name: string;
  PA: number;
  value: number;
  strong?: boolean;
};

function Row({ number, name, value, strong }: RowProps) {
  return (
    <TableRow key={name}>
      <TableCell
        style={{ padding: 4, borderBottomStyle: 'none', fontWeight: strong ? 'bold' : undefined }}
        align="right"
        component="th"
        scope="row"
        padding="none"
      >
        # {number}
      </TableCell>
      <TableCell style={{ padding: 4, borderBottomStyle: 'none', fontWeight: strong ? 'bold' : undefined }} padding="none">
        {name}
      </TableCell>
      <TableCell style={{ padding: 4, borderBottomStyle: 'none', fontWeight: strong ? 'bold' : undefined }} align="right" padding="none">
        {value}
      </TableCell>
    </TableRow>
  );
}
